#!/usr/bin/env python3

import yaml
import json
import sys
from pathlib import Path
from collections.abc import Iterable

# Import src
#sys.path.append(str(Path(__file__).parent / "src"))

# App modules
from faial.traverse import *
from faial import clib
from faial.c_ast import is_kind
from faial.access import rewrite_access
from faial.loops import rewrite_loops
from faial.cuda import FILTER_CUDA, rewrite_cuda
from faial.unsupported import rewrite_unsupported

class Visitor:
    def __init__(self):
        self.dispatchers = {}
        for k, v in self.__dict__:
            if k.startswith("on_"):
                self.dispatchers[len("on_"):] = v

    def dispatch(self, obj):
        return self.dispatchers[obj["kind"]](obj)

class Simpl(Visitor):
    def on_CompoundStmt(self, obj):
        for child in obj["inner"]:
            self.dispatch(child)

def mk_clean(args):
    def clean(data):
        return clib.clean_c(data,
            key_filter=clib.FILTER_KEY_LOCATION if args.remove_source else set(),
            record_filter=FILTER_CUDA,
        )
    return clean

def mk_access(args):
    if args.skip_access:
        def f(data):
            pass
        return f
    else:
        return rewrite_access

# This represents our data-cleaning pipeline
PIPELINE = [
    mk_clean,
    lambda args: rewrite_cuda,
    mk_access,
    lambda args: rewrite_loops,
    lambda args: rewrite_unsupported,
]

def build_pipeline(pipeline, args):
    return list(p(args) for p in pipeline)

def run_pipeline(pipeline, data):
    """
    Changes to the data are *in-place*
    """
    # Run the pipeline
    for pipe in pipeline:
        pipe(data)

def main():
    import argparse
    parser = argparse.ArgumentParser(prog="faial-infer", description='Infer a protocol.')
    parser.add_argument('filename',  help='A C-JSON source file.')
    parser.add_argument("--skip-access", action="store_true", help="Abstract memory accesses (default: true).")
    parser.add_argument("--provenance", action='store_false', dest='remove_source',  help='Include source provenance.')
    parser.add_argument("-o", dest="output", help="Outputs to a file. By default prints to the standard output.")
    parser.add_argument("-t", dest="type", choices=["json", "yaml"], default="yaml", help="Specifies the serialization format. Default: %(default)s")
    parser.add_argument("-X", dest="steps", type=int, default=len(PIPELINE), help="Run n-steps of the pipeline (for debugging purposes). Default: %(default)s")
    args = parser.parse_args()
    if args.filename.strip() == "-":
        data = json.load(sys.stdin)
    else:
        with open(args.filename) as fp:
            try:
                data = json.load(fp)
            except json.decoder.JSONDecodeError as e:
                print(f"Error parsing '{args.filename}':", e, file=sys.stderr)
                sys.exit(255)
    # Build the first N-steps of the pipeline
    pipeline = build_pipeline(PIPELINE, args)[:args.steps]

    dumpers = {
        "json": json.dump,
        "yaml": yaml.dump,
    }
    dump = dumpers[args.type]

    # Run the pipeline
    run_pipeline(pipeline, data)

    if args.output is None:
        dump(data, sys.stdout)
    else:
        with open(args.output, "w") as fp:
            dump(data, fp)

if __name__ == '__main__':
    main()
