FROM ubuntu:20.04
ARG DEBIAN_FRONTEND="noninteractive"
ENV TZ="America/New_York"
RUN apt-get update && \
    apt-get install -y \
      libssl-dev \
      build-essential \
      pkg-config \
      wget \
      && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN useradd -m acme
USER acme

# Install Rust
ARG RUST_VERSION=1.47.0
ENV PATH="$PATH:/home/acme/.cargo/bin"
RUN wget https://sh.rustup.rs -O - | \
    sh -s -- -y --default-toolchain ${RUST_VERSION} --profile minimal

# faial-infer pre-reqs
RUN cargo install pyoxidizer --version 0.8.0
