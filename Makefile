POETRY = poetry
PYOXIDIZER = pyoxidizer
GITLAB_CACHE = /tmp/gitlab-cache

all:

sdist:
	$(POETRY) build

deps:
	$(POETRY) install

test:
	$(POETRY) run pytest

gitlab-test:
	 gitlab-runner exec docker test --cache-dir=${GITLAB_CACHE} --docker-cache-dir=${GITLAB_CACHE} --docker-volumes=${GITLAB_CACHE}

gitlab-bin:
	 gitlab-runner exec docker bin --cache-dir=${GITLAB_CACHE} --docker-cache-dir=${GITLAB_CACHE} --docker-volumes=${GITLAB_CACHE}

gitlab: gitlab-bin gitlab-test


bin:
	$(PYOXIDIZER) build --release

.PHONY: all deps test gitlab-test gitlab-bin gitlab